#include <string.h>
#include "timer.h"

/************************************************************************
 * Class Implementation
 ************************************************************************/

companion::Timer::Timer(void)
{
  period_ms = 0;
  started = false;
}

companion::Timer::Timer(uint ms)
{
  period(ms);
}

bool companion::Timer::valid()
{
  return started;
}

void companion::Timer::reset(void)
{
  clock_gettime(CLOCK_MONOTONIC, &t0);
  started = true;
}

bool companion::Timer::expired(uint* elapsed_ptr, bool autoreset)
{
  uint _elapsed = elapsed();
  bool ret = (_elapsed >= period_ms);
  if (ret && autoreset) reset();
  if (elapsed_ptr) *elapsed_ptr = _elapsed;
  return ret;
}

uint companion::Timer::elapsed(void) const
{
  timespec t;
  clock_gettime(CLOCK_MONOTONIC, &t);
  return (Timer::difference(t, t0));
}

uint companion::Timer::period(void) const
{
  return period_ms;
}

void companion::Timer::period(uint ms)
{
  period_ms = ms;
  reset();
}

/* performs a - b, assumes a >= b */
uint companion::Timer::difference(const timespec& a, const timespec& b)
{
  DEBUGASSERT(a.tv_sec >= b.tv_sec);

  uint d = (a.tv_sec - b.tv_sec) * 1000;

  if (a.tv_nsec < b.tv_nsec)
  {
    d -= 1000;
		d += ((1000000000 + a.tv_nsec) - b.tv_nsec) / 1000000;
  }
  else
  {
    d += (a.tv_nsec - b.tv_nsec) / 1000000;
  }

	return d;
}
