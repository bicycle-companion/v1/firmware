//***************************************************************************
// Included Files
//***************************************************************************

#include <nuttx/config.h>
#include <cstdio>
#include <debug.h>
#include <nuttx/init.h>
#include <nuttx/arch.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <string.h>
#include <arch/board/board.h>
#include <nuttx/lcd/lcd.h>
#include <nuttx/board.h>
#include <errno.h>
#include <stdlib.h>
#include <nuttx/analog/adc.h>

#include <nuttx/nx/nx.h>
#include <nuttx/nx/nxglib.h>
#include <nuttx/nx/nxfonts.h>
#include <semaphore.h>
#include <sys/time.h>
#include <stdio.h>

#include "components.h"
#include "timer.h"

//***************************************************************************
// Definitions
//***************************************************************************

//***************************************************************************
// Global Variables
//***************************************************************************

companion::Components* companion::components;

//***************************************************************************
// Auxiliary Functions
//***************************************************************************

void display_draw(companion::Display& display, const char* str, float progress)
{
#if 0
  nxgl_size_s display_size = display.size;
  nxgl_coord_t last_x = display_size.w - 1;
  nxgl_coord_t last_y = display_size.h - 1;

  display.line({ { 0, 0 }, { 0, last_y } });
  display.line({ { 0, 0 }, { last_x, 0 } });
  display.line({ { last_x, 0 }, { last_x, last_y } });
  display.line({ { 0, last_y }, { last_x, last_y } });

  display.print(str, {2,2}, companion::Display::Font::MONO_SMALL);
  //display.print("45", {2,20}, companion::Display::Font::SANS_BIG);

  //printf("p: %i\n", (nxgl_coord_t)(last_x * progress));
  display.fill({{0,last_y - 3},{(nxgl_coord_t)(last_x * progress), last_y}});
  display.fill({{(nxgl_coord_t)(last_x * progress + 1),last_y - 3},{last_x, last_y}}, false);
#endif
}


//***************************************************************************
// Callbacks Functions
//***************************************************************************




//***************************************************************************
// Main Task
//***************************************************************************

#include "bitmaps/battery_charging.xbm"
#include "bitmaps/battery_critical.xbm"
#include "bitmaps/battery_empty.xbm"

extern "C"
int companion_main(int argc, char *argv[])
{
  companion::Components components;
  companion::components = &components;

  /* TODO: make this into a constructor */
  components.battery.initialize();
  components.input.initialize();
  components.display.initialize();
  components.gui.initialize();
  components.odometer.initialize();

#if 1
  while(true)
  {
    components.input.loop();
    components.battery.loop();
    components.odometer.loop();
    components.gui.loop();

    //printf("hall: %i\n", components.input.button_state(companion::Input::Button::HALL));
    //printf("wheel velocity: %.3f\n", components.odometer.speed(companion::Odometer::Units::M_S));
    //components.battery.loop();




#if 0
    /* debug */
    printf("charging?: %i %.3f\n", !(components.input.button_state(companion::Input::Button::CHARGER)),
                                 components.battery.voltage());
#endif
  }


#else
  nxgl_rect_s rect;
  rect.pt1.x = 16;
  rect.pt1.y = 10;
  rect.pt2.x = rect.pt1.x + (battery_critical_width - 1);
  rect.pt2.y = rect.pt1.y + (battery_critical_height - 1);

  float progress = 0;
  int direction = 1;
  while(true)
  {
    components.input.loop();

    printf("buttons: %i %i %i %i\n",
           components.input.button_state(companion::Input::Button::DOWN),
           components.input.button_state(companion::Input::Button::UP),
           components.input.button_state(companion::Input::Button::LEFT),
           components.input.button_state(companion::Input::Button::RIGHT)
           );

    /* get time */
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    char time_str[16];
    sprintf(time_str, "%.2i:%.2i:%.2i", tm.tm_hour, tm.tm_min, tm.tm_sec);

    display_draw(components.display, time_str, progress);

    components.display.print("100", {0,12}, companion::Display::Font::MONO_SMALL);
    components.display.bitmap((const uint8_t*)battery_critical_bits, rect, rect.pt1);

#if 1
    if (progress >= 1.0)
    {
      direction = -1;
    }
    else if (progress <= 0)
    {
      direction = 1;
    }

    progress += direction * 0.01;
#else
    direction = -direction;
    progress = (direction == 1 ? 0 : 1);
#endif

    components.display.clear();
  }
#endif

  return 0;
}
