#ifndef BATTERY_H
#define BATTERY_H

#include <nuttx/config.h>
#include <cstdio>
#include <debug.h>
#include <nuttx/init.h>
#include <nuttx/arch.h>
#include <time.h>
#include <fcntl.h>
#include <arch/board/board.h>
#include <nuttx/board.h>
#include <errno.h>
#include <stdlib.h>

namespace companion
{
  class Battery
  {
    public:
      Battery(void);
      ~Battery(void);

      bool initialize(void);
      float voltage(void);
      float percentage(void);
      bool charging(void);

      void loop(void);

    private:
      int fd;
      bool measuring;
      float _voltage;
      float charge_level;
      int display_charge_level;

      bool start_measure(void);
      void check_voltage(void);
  };
}

#endif
