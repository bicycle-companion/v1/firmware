#ifndef TIMER_H
#define TIMER_H

#include <nuttx/config.h>
#include <cstdio>
#include <debug.h>
#include <nuttx/init.h>
#include <nuttx/arch.h>
#include <time.h>
#include <fcntl.h>
#include <arch/board/board.h>
#include <nuttx/board.h>
#include <errno.h>
#include <stdlib.h>
#include <time.h>

namespace companion
{
  class Timer
  {
    public:
      Timer(void);
      Timer(uint ms);

      bool valid(void);
      void reset(void);
      bool expired(uint* elapsed = NULL, bool autoreset = true);
      uint elapsed(void) const;
      uint period(void) const;
      void period(uint ms);

    private:
      uint period_ms;
      timespec t0;
      bool started;

      static uint difference(const timespec& a, const timespec& b); /* compute (a-b) difference in ms */
  };
}

#endif // TIMER_H
