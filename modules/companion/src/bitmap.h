#ifndef BITMAP_H
#define BITMAP_H

#include <nuttx/config.h>
#include <cstdio>
#include <debug.h>
#include <nuttx/init.h>
#include <nuttx/arch.h>
#include <time.h>
#include <fcntl.h>
#include <arch/board/board.h>
#include <nuttx/lcd/lcd.h>
#include <nuttx/board.h>
#include <errno.h>
#include <stdlib.h>

#include <nuttx/nx/nx.h>
#include <nuttx/nx/nxglib.h>
#include <nuttx/nx/nxfonts.h>

namespace companion
{
  class Bitmap
  {
    public:
      enum class Orientation { BITS_ARE_COLUMNS, BITS_ARE_ROWS };

      Bitmap(void) = delete;

      Bitmap(const uint8_t* buffer, uint32_t width_bytes, uint32_t width,
             uint32_t height, uint32_t xoffset = 0, uint32_t yoffset = 0);

      Bitmap(const nxgl_size_s& s);
      Bitmap(const Bitmap& other);

      ~Bitmap(void);

      void copy_into(Bitmap& b, const nxgl_point_s& pos) const;
      const nxgl_size_s& size(void) const;
      const nxgl_point_s& offset(void) const;
      nxgl_size_s real_size(void) const;
      nxgl_rect_s extents(void) const;
      nxgl_rect_s full_extents(void) const;

      const uint8_t* buffer(void) const;
      uint8_t* buffer(void);

      int stride(void) const;
      size_t bufsize(void) const;

      void fill(bool color = 1);

    private:
      nxgl_size_s pixel_size;
      int _stride, buffer_size;
      uint8_t* _buffer;
      nxgl_point_s _offset;
  };
}

#endif // BITMAP_H
