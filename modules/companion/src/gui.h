#ifndef GUI_H
#define GUI_H

#include <nuttx/config.h>
#include <cstdio>
#include <debug.h>
#include <nuttx/init.h>
#include <nuttx/arch.h>
#include <time.h>
#include <fcntl.h>
#include <arch/board/board.h>
#include <nuttx/board.h>
#include <errno.h>
#include <stdlib.h>
#include "timer.h"

namespace companion
{
  class Gui
  {
    public:
      Gui(void);
      ~Gui(void);

      enum Page { SPLASH = 0, ODOMETER, TIME, COUNT };

      bool initialize(void);
      void loop(void);

    private:
      void run_fsm(void);

      void powersaver(void);
      Timer powersave_timer;

      /* pages */
      void on_page_splash(void);
      void on_page_odometer(void);
      void on_page_time(void);

      typedef void (Gui::*PageCallback)(void);

      Timer page_timer;
      Page current_page;
      bool redraw;

      /* page data */
      struct {
        time_t t;
      } page_time_data;

      /* debug */
      int i, j;
  };
}

#endif // GUI_H
