#ifndef __COMPONENTS_H__
#define __COMPONENTS_H__

#include "display.h"
#include "gui.h"
#include "input.h"
#include "battery.h"
#include "odometer.h"

namespace companion
{
  struct Components
  {
    Display display;
    Battery battery;
    Input input;
    Gui gui;
    Odometer odometer;
  };

  extern Components* components;
}

#endif
