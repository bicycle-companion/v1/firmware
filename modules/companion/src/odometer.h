#ifndef ODOMETER_H
#define ODOMETER_H

#include <nuttx/config.h>
#include <cstdio>
#include <debug.h>
#include <nuttx/init.h>
#include <nuttx/arch.h>
#include <time.h>
#include <fcntl.h>
#include <arch/board/board.h>
#include <nuttx/board.h>
#include <errno.h>
#include <stdlib.h>
#include "timer.h"

namespace companion
{
  class Odometer
  {
    public:
      enum class Units { KM_H, M_S };

      Odometer(void);
      ~Odometer(void);

      bool initialize(void);
      void loop(void);
      float speed(Units units = Units::KM_H);

    private:
      Timer delta_t;
      uint ticks;
      bool hall_state;
      float tick_speed; // [ticks/s]
  };
}

#endif // INPUT_H
