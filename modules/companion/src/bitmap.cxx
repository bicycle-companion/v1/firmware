#include <string.h>
#include "bitmap.h"

#  define NXGL_PIXELSHIFT          3
#  define NXGL_PIXELMASK           7
#  define NXGL_SCALEX(x)           ((x) >> NXGL_PIXELSHIFT)
#  define NXGL_REMAINDERX(x)       ((x) & NXGL_PIXELMASK)

companion::Bitmap::Bitmap(const uint8_t* in_buffer, uint32_t width_bytes, uint32_t width,
                          uint32_t height, uint32_t xoffset, uint32_t yoffset)
  : Bitmap({ width, height })
{
  _offset = { xoffset, yoffset };
  memset(_buffer, 0x00, buffer_size);

  //printf("built: %i %i %i %i %i\n", width_bytes, width, height, _stride, buffer_size);

  uint8_t out_mask = (1 << 0);
  uint8_t in_idx, out_idx;
  in_idx = out_idx = 0;

  /* copy input buffer, transposing rows-columns to match display */
  for (uint32_t i = 0; i < height; i++)
  {
    /* always start input rows with mask in MSB */
    uint8_t in_mask = (1 << 7);

    for (uint32_t j = 0; j < width; j++)
    {
      if (out_idx >= buffer_size)
      {
        out_idx = i >> 3;        /* move down one row by choosing the next bit
                                    of the byte corresponding to the current row */
        out_mask <<= 1;
        if (!out_mask) out_mask = (1 << 0);
      }

      /*printf("in_idx: %i in_mask: %x out_idx: %i out_mask: %x bit: %i j %i i %i\n", in_idx, in_mask, out_idx, out_mask,
             in_buffer[in_idx] & in_mask ? 1 : 0, j, i);*/
      /* if input bit is on, turn on output bit */
      if (in_buffer[in_idx] & in_mask)
        _buffer[out_idx] |= out_mask;

      in_mask >>= 1; /* advance one column by moving one bit */
      if (!in_mask) { in_mask = (1 << 7); in_idx++; } /* move to the next byte */

      out_idx += _stride; /* advance one column by moving one stride of bytes */
    }

    if (in_mask != (1 << 7)) in_idx++; /* in case we ended last byte before reaching last bit, advance next byte */
  }
}

companion::Bitmap::Bitmap(const nxgl_size_s& s)
{
  DEBUGASSERT(s.w > 0 && s.h > 0);

  _stride = (s.h + 7) >> 3;
  buffer_size = _stride * s.w;
  pixel_size = s;
  _buffer = (uint8_t*)malloc(buffer_size);
  _offset = { 0, 0 };
}

companion::Bitmap::Bitmap(const companion::Bitmap& other)
{
  Bitmap(other.size());
  memcpy(buffer(), other.buffer(), buffer_size);
}

companion::Bitmap::~Bitmap()
{
  free(_buffer);
}

const nxgl_size_s&companion::Bitmap::size(void) const
{
  return pixel_size;
}

const nxgl_point_s&companion::Bitmap::offset() const
{
  return _offset;
}

nxgl_size_s companion::Bitmap::real_size() const
{
  return { pixel_size.w - _offset.x, pixel_size.h - _offset.y };
}

/**
 * returns a rectangle describing the actual bitmap region inside the
 * image, w.r.t. top-left corner of bitmap
 */
nxgl_rect_s companion::Bitmap::extents() const
{
  return { { _offset.x, _offset.y }, { pixel_size.w - 1, pixel_size.h - 1 } };
}

nxgl_rect_s companion::Bitmap::full_extents() const
{
  return { { 0, 0 }, { pixel_size.w - 1, pixel_size.h - 1 } };
}

const uint8_t*companion::Bitmap::buffer(void) const
{
  return _buffer;
}

uint8_t* companion::Bitmap::buffer(void)
{
  return _buffer;
}

int companion::Bitmap::stride(void) const
{
  return _stride;
}

size_t companion::Bitmap::bufsize() const
{
  return buffer_size;
}

void companion::Bitmap::fill(bool color)
{
  memset(_buffer, color ? 0xFF : 0x00, buffer_size);
}

void companion::Bitmap::copy_into(companion::Bitmap& dest, const nxgl_point_s& pos) const
{
}
