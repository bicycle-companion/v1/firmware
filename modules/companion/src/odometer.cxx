#include <fcntl.h>
#include <string.h>
#include <math.h>
#include "components.h"

/**
 * TODO:
 * - use interrupt-based counter (even better: input capture)
 * - better sampling timing (solved by above)
 * - running average? low pass?
 */

/************************************************************************
 * Global constants
 ************************************************************************/

#define ODOMETER_SAMPLE_PERIOD 5000 // [ms]
#define LINEAR_DISTANCE_PER_REVOLUTION 5 // this is for test wheel
//#define LINEAR_DISTANCE_PER_REVOLUTION 2.2f // [m] TODO: make this configurable, this is for 28'' wheel

/************************************************************************
 * Function declarations
 ************************************************************************/


/************************************************************************
 * Class Implementation
 ************************************************************************/

companion::Odometer::Odometer(void)
{
  ticks = 0;
  tick_speed = 0;
}

companion::Odometer::~Odometer(void)
{
}

bool companion::Odometer::initialize(void)
{
  hall_state = companion::components->input.button_state(companion::Input::Button::HALL);
  return true;
}

void companion::Odometer::loop(void)
{
  /* update ticks */
  bool new_hall_state = companion::components->input.button_state(companion::Input::Button::HALL);

  if (new_hall_state != hall_state)
  {
    hall_state = new_hall_state;
    if (hall_state) /* one revolution is complete when sensor goes back to either state (here I use HIGH) */
    {
      ticks++; /* add one revolution */
    }
  }

  /* periodically update speed */
  if (!delta_t.valid()) delta_t.period(ODOMETER_SAMPLE_PERIOD);
  else
  {
    uint elapsed_time;
    if (delta_t.expired(&elapsed_time))
    {
      tick_speed = ticks / (elapsed_time / 1000.0f); /* TODO: use integer math?, use low pass filter? */
      printf("speed: %.3f\n", tick_speed);
      ticks = 0;
    }
  }
}

float companion::Odometer::speed(companion::Odometer::Units units)
{
  if (units == Units::KM_H)
  {
    return roundf(tick_speed * LINEAR_DISTANCE_PER_REVOLUTION / 1e3f); // m -> km
  }
  else
  {
    return roundf(tick_speed * LINEAR_DISTANCE_PER_REVOLUTION); // ms -> s
  }
}
