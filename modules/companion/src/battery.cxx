#include <stdio.h>
#include <nuttx/analog/adc.h>
#include <sys/ioctl.h>
#include <arch/board/board.h>
#include <math.h>
#include "components.h"

/************************************************************************
 * Global definitions
 ************************************************************************/

#define BATTERY_SCALE_FACTOR 2.0f// inverse of resistor divider ratio

#define BATTERY_MAX_VOLTS 4.2f   // Full charge
#define BATTERY_LOW_VOLTS 3.7f   // Desired charge point
#define BATTERY_MIN_VOLTS 3.6f   // Considered as extremely low (this should still be a safe value)


/************************************************************************
 * Class Implementation
 ************************************************************************/

companion::Battery::Battery(void) :
  fd(-1), measuring(false), _voltage(0), charge_level(0),
  display_charge_level(0)
{

}

companion::Battery::~Battery(void)
{

}

bool companion::Battery::initialize(void)
{
  fd = open("/dev/adc0", O_BINARY | O_RDONLY | O_NONBLOCK);
  if (fd < 0) { fprintf(stderr, "failed to open ADC0\n"); return false; }

  if (!start_measure()) return false;

  return true;
}

float companion::Battery::voltage(void)
{
  return _voltage;
}

float companion::Battery::percentage(void)
{
  if (_voltage < BATTERY_MIN_VOLTS) return 0;
  else if (_voltage > BATTERY_MAX_VOLTS) return 100;
  else return (_voltage - BATTERY_MIN_VOLTS) / (BATTERY_MAX_VOLTS - BATTERY_MIN_VOLTS) * 100.0f;
}

bool companion::Battery::charging(void)
{
  return !companion::components->input.button_state(companion::Input::Button::CHARGER);
}

void companion::Battery::loop(void)
{
  check_voltage();
  printf("charging: %s percentage: %i voltage: %.3f\n", charging() ? "yes" : "no", (int)percentage(), voltage());
}

bool companion::Battery::start_measure(void)
{
  if (ioctl(fd, ANIOC_TRIGGER, ADC_CHANNEL_A0) < 0)
  {
    fprintf(stderr, "failed to initiate battery measurement\n");
    return false;
  }
  else
  {
    //printf("triggering\n");
    measuring = true;
    return true;
  }
}

void companion::Battery::check_voltage(void)
{
  if (measuring)
  {
    adc_msg_s msg;

    /* get ADC result */
    if (read(fd, &msg, sizeof(msg)) < 0) /* if measurement not done yet */
    {
      return;
    }
    else
    {
      printf("raw: %i\n", msg.am_data);

      _voltage = (msg.am_data / 65535.0) * 3.3 * BATTERY_SCALE_FACTOR; /* convert to V (TODO: use actual VREFHIGH and use resistor divider ratio) */

      /* compute charge level from voltage */
      /* TODO, update charge_level */

      /* check if the displayed version (rounded) of the charge level needs to be updated,
       * if so, redraw the display */
      int display_charge_level_old = display_charge_level;
      display_charge_level = (int)ceilf(charge_level);
      if (display_charge_level != display_charge_level_old)
      {
        /* issue redraw */
        /* TODO: this is just preliminary, in final version this class should be passive and a GUI class
         * manage drawing since it would depend on current view. thus this check would be external to this class */
      }

      measuring = false;
    }
  }

  /* (re-)initiate measurement */
  start_measure();
  usleep(500); /* hack: this delay seems to avoid a race condition when starting a new measure too recently */
}
