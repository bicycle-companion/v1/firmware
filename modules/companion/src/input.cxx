#include <fcntl.h>
#include <nuttx/input/buttons.h>
#include <string.h>
#include "input.h"

/************************************************************************
 * Function declarations
 ************************************************************************/


/************************************************************************
 * Class Implementation
 ************************************************************************/

companion::Input::Input(void)
{
  fd = -1;
  buttons = 0;
}

companion::Input::~Input(void)
{
  if (fd != -1) close(fd);
}

bool companion::Input::initialize(void)
{
  fd = open("/dev/buttons", O_BINARY | O_RDONLY);
  if (fd < 0)
  {
    fprintf(stderr, "Failed to open buttons device\n");
    return false;
  }

  return true;
}

void companion::Input::loop(void)
{
  int ret = read(fd, &buttons, sizeof(buttons));
  if (ret < 0)
  {
    fprintf(stderr, "Failed to read buttons: %s\n", strerror(errno));
    return;
  }
}



bool companion::Input::button_state(companion::Input::Button b)
{
  return ((buttons & (btn_buttonset_t)b) ? true : false);
}


/**********************************************************************
 * Static function implementation
 **********************************************************************/

