#ifndef INPUT_H
#define INPUT_H

#include <nuttx/config.h>
#include <cstdio>
#include <debug.h>
#include <nuttx/init.h>
#include <nuttx/arch.h>
#include <time.h>
#include <fcntl.h>
#include <arch/board/board.h>
#include <nuttx/board.h>
#include <errno.h>
#include <stdlib.h>
#include <nuttx/input/buttons.h>

namespace companion
{
  class Input
  {
    public:
      enum class Button { UP = BUTTON_1_BIT,
                          DOWN = BUTTON_2_BIT,
                          LEFT = BUTTON_3_BIT,
                          RIGHT = BUTTON_4_BIT,
                          HALL = BUTTON_5_BIT,
                          CHARGER = BUTTON_6_BIT};

      Input(void);
      ~Input(void);

      bool initialize(void);
      void loop(void);

      bool button_state(Button b);

    private:
      int fd;
      btn_buttonset_t buttons;
  };
}

#endif // INPUT_H
