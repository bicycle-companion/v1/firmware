#include <string.h>
#include "components.h"
#include "bitmap.h"

/************************************************************************
 * Global constants
 ************************************************************************/

#define SPLASH_TIMER 1000 // ms
#define POWERSAVE_TIMER 60000 // ms

/************************************************************************
 * Class Implementation
 ************************************************************************/

companion::Gui::Gui(void) :
  page_timer(SPLASH_TIMER),
  powersave_timer(POWERSAVE_TIMER)
{
  current_page = Page::SPLASH;
  redraw = true;

  i = j = 0;
}

companion::Gui::~Gui(void)
{
}

void companion::Gui::run_fsm(void)
{
  Input& input = companion::components->input;
  Display& display = companion::components->display;

  /* if display is off due to power save do not update GUI nor process buttons */
  powersaver();
  if (!display.power()) return;

  Page page_before = current_page;

  switch(current_page)
  {
    case Page::SPLASH:
      on_page_splash();
    break;
    case Page::ODOMETER:
      on_page_odometer();
    break;
    case Page::TIME:
      on_page_time();
    break;
    default:
    break;
  }

  if (page_before != current_page)
  {
    display.clear_memory();
    redraw = true;
  }
}

void companion::Gui::powersaver(void)
{
  if (companion::components->input.button_state(companion::Input::Button::DOWN) ||
      companion::components->input.button_state(companion::Input::Button::UP)   ||
      companion::components->input.button_state(companion::Input::Button::LEFT) ||
      companion::components->input.button_state(companion::Input::Button::RIGHT))
  {
    powersave_timer.reset();

    if (!companion::components->display.power())
      companion::components->display.power(true);
  }
  else if (companion::components->display.power() && powersave_timer.expired(NULL, false))
  {
    companion::components->display.power(false);
  }
}

bool companion::Gui::initialize(void)
{
  return true;
}

void companion::Gui::loop(void)
{
  run_fsm();

  /* handle button inputs and Gui page state machine (time-based, etc) */
}

/******** Pages *********/

void companion::Gui::on_page_splash(void)
{
  if (redraw)
  {
    companion::components->display.print("Bicycle", {0,0}, companion::Display::Font::SANS_MEDIUM);
    companion::components->display.print("Companion", {13,15}, companion::Display::Font::SANS_MEDIUM);
    companion::components->display.refresh();
    redraw = false;
  }

  if (page_timer.expired())
  {
    current_page = Page::ODOMETER;
    printf("-> odometer\n");
  }
}

void companion::Gui::on_page_odometer(void)
{
  if (redraw)
  {
    char speed_str[3];
    sprintf(speed_str, "%.2i", (int)(companion::components->odometer.speed(companion::Odometer::Units::M_S)));

    nxgl_rect_s bb = companion::components->display.print(speed_str, {50,16}, companion::Display::Font::SANS_BIG);
    companion::components->display.print("m/s", {bb.pt2.x + 3,22}, companion::Display::Font::SANS_MEDIUM);
    companion::components->display.refresh();
    companion::components->display.clear_memory();
  }

  if (companion::components->input.button_state(Input::Button::RIGHT))
  {
    printf("-> time\n");
    current_page = Page::TIME;
  }
}

void companion::Gui::on_page_time(void)
{
  if (redraw)
  {    
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    char time_str[16];
    sprintf(time_str, "%02.2i:%02.2i", tm.tm_hour, tm.tm_min);
    nxgl_rect_s bb = companion::components->display.print(time_str, {0,0}, companion::Display::Font::SANS_BIG);
    sprintf(time_str, "%02.2i", tm.tm_sec);

    bb = companion::components->display.print(time_str, {bb.pt2.x + 3, bb.pt2.y - 8}, companion::Display::Font::SANS_MEDIUM);

    companion::components->display.refresh();
    companion::components->display.clear_memory();
    //redraw = false;
  }

  if (companion::components->input.button_state(Input::Button::LEFT))
  {
    printf("-> odometer\n");
    current_page = Page::ODOMETER;
  }
}
