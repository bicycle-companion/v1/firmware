#ifndef DISPLAY_H
#define DISPLAY_H

#include <nuttx/config.h>
#include <cstdio>
#include <debug.h>
#include <nuttx/init.h>
#include <nuttx/arch.h>
#include <time.h>
#include <fcntl.h>
#include <arch/board/board.h>
#include <nuttx/video/fb.h>
#include <nuttx/board.h>
#include <errno.h>
#include <stdlib.h>

#include <nuttx/nx/nx.h>
#include <nuttx/nx/nxglib.h>
#include <nuttx/nx/nxfonts.h>

#include "bitmap.h"

namespace companion
{
  class Display
  {
    public:
      enum class Font { SANS_MEDIUM = 0, SANS_BIG, MONO_SMALL, FONTS_NUM };
      enum class Color { BLACK, WHITE, INVERT };

      Display(void);
      ~Display(void);

      bool initialize(void);

      void contrast(uint8_t _contrast);
      void power(bool on);
      bool power(void);

      void pixel(nxgl_point_s point, Color color = Color::BLACK);
      void hline(nxgl_point_s start, nxgl_coord_t x_end, Color color = Color::BLACK);
      void vline(nxgl_point_s start, nxgl_coord_t y_end, Color color = Color::BLACK);
      void fill(const nxgl_rect_s& rect, Color color = Color::BLACK);
      void rect(const nxgl_rect_s& rect, Color color = Color::BLACK);
      void line(nxgl_vector_s vect, Color color = Color::BLACK);
      nxgl_rect_s print(const char* text, nxgl_point_s pos, Font font, Color color = Color::BLACK);
      void bitmap(const uint8_t* data, const nxgl_rect_s& rect, const nxgl_point_s& origin);
      void circle(const nxgl_point_s pos, nxgl_coord_t radius, nxgl_coord_t start_section = 0,
                  nxgl_coord_t end_section = 16, Color color = Color::BLACK);
      void filledcircle(const nxgl_point_s pos, nxgl_coord_t radius, nxgl_coord_t start_section = 0,
                        nxgl_coord_t end_section = 16, Color color = Color::BLACK);
      void trapezoid(const nxgl_trapezoid_s& trap, Color color = Color::BLACK);

      void clear_memory(void);
      void clear(void);
      void refresh(void);

      uint8_t* framebuffer(void);

      void bitmap(const Bitmap& bitmap, const nxgl_point_s& pos, nxgl_rect_s* bounding_box);

      void debug(void);

      nxgl_size_s size;
      nxgl_rect_s screen_rect;

    private:
      struct nx_callback_s nx_callback;

      void apply_color(uint8_t* ptr, Color color, int mask);
      void apply_color_n(uint8_t* ptr, Color color, size_t n);


      void load_fonts(void);
      NXHANDLE font_handles[(int)Font::FONTS_NUM];

      FAR struct fb_vtable_s* fb_dev;
      uint8_t* fbmem;
      int buffer_size;
      int stride;

      bool _power;
  };
}

#endif // DISPLAY_H
