#include <nuttx/lcd/ssd1306.h>
#include <string.h>
#include "display.h"

/************************************************************************
 * Class Implementation
 ************************************************************************/

companion::Display::Display(void)
{
  _power = true;
}

companion::Display::~Display(void)
{
}

bool companion::Display::initialize()
{
  /* initialize LCD */
  if (up_fbinitialize(0) < 0)
  {
    fprintf(stderr, "up_fbinitialize failed\n");
    return false;
  }

  /* Get the device instance */
  fb_dev = up_fbgetvplane(0, 0);
  if (!fb_dev )
  {
    fprintf(stderr,"up_fbgetvplane failed\n");
    return false;
  }

  /* Get plane information */
  fb_planeinfo_s planeinfo;
  fb_dev->getplaneinfo(fb_dev, 0, &planeinfo);
  fbmem = (uint8_t*)planeinfo.fbmem;
  stride = planeinfo.stride;
  buffer_size = planeinfo.fblen;

  /* Get video information */
  fb_videoinfo_s videoinfo;
  fb_dev->getvideoinfo(fb_dev, &videoinfo);
  size.w = videoinfo.xres;
  size.h = videoinfo.yres;
  screen_rect.pt1 = { 0, 0 };
  screen_rect.pt2 = { size.w - 1, size.h - 1 };

  /* Load font handles */
  load_fonts();

  printf("Display initialized (w: %i h: %i)\n", size.w, size.h);

  return true;
}

void companion::Display::contrast(uint8_t _contrast)
{
  up_fbsetcontrast(0, _contrast);
}

void companion::Display::power(bool on)
{
  _power = on;
  up_fbsetpower(0, on);
}

bool companion::Display::power()
{
  return _power;
}

void companion::Display::apply_color(uint8_t* ptr, companion::Display::Color color, int mask)
{
  switch(color)
  {
    case Color::BLACK:
      *ptr |= mask;
    break;
    case Color::WHITE:
      *ptr &= ~mask;
    break;
    case Color::INVERT:
      *ptr ^= mask;
    break;
  }
}

void companion::Display::apply_color_n(uint8_t* ptr, companion::Display::Color color, size_t n)
{
  if (color == Color::INVERT)
    while (n--) { *ptr ^= 0xFF; ++ptr; }
  else
    memset(ptr, (color == Color::BLACK ? 0xFF : 0x00), n);
}

void companion::Display::pixel(nxgl_point_s point, Color color)
{
  /* TODO: bounds check */
  uint8_t* dest = fbmem + point.x * stride + (point.y >> 3);

  int shift   = (point.y & 7);                   /* Get bit position inside byte */
  int mask    = (1 << shift);                    /* Mask to choose corresponding bit */

  apply_color(dest, color, mask);
}

void companion::Display::hline(nxgl_point_s start, nxgl_coord_t x_end, companion::Display::Color color)
{
  /* TODO: bounds check */
  uint8_t* dest = fbmem + start.x * stride + (start.y >> 3);

  int shift   = (start.y & 7);                   /* Get bit position inside byte */
  int mask    = (1 << shift);                    /* Mask to choose corresponding bit */

  for (nxgl_coord_t i = start.x; i <= x_end; i++, dest += stride)
  {
    apply_color(dest, color, mask);
  }
}

void companion::Display::vline(nxgl_point_s start, nxgl_coord_t y_end, companion::Display::Color color)
{
  /* TODO: bounds check */
  uint8_t* dest = fbmem + start.x * stride + (start.y >> 3);

  int mod_start = (start.y & 7);     /* how many bits into the first byte does the line start */
  int mod_end = (y_end & 7);         /* how many bits into the last byte does the line end */
  int length = y_end - start.y + 1;

  int leadmask = (0xFF << mod_start); /* fill with 1s starting from the first bit (reverse, since it goes LSB->MSB) */
  int endmask = 0xFF >> (7 - mod_end); /* fill with 1s until last bit (reverse, since it goes LSB->MSB) */

  if ((start.y >> 3) == (y_end >> 3)) /* line falls within one byte */
  {
    int mask = (leadmask & endmask); /* first and last byte are the same, combine both masks */
    apply_color(dest, color, mask);
  }
  else
  {
    /* apply color in first set of bits */
    apply_color(dest, color, leadmask);
    ++dest;

    length -= (8 - mod_start);

    /* apply color in bytes with all bits affected */
    int full_bytes = length >> 3;
    apply_color_n(dest, color, full_bytes);
    dest += full_bytes;

    if (length & 7)
    {
      /* apply color in last set of bits */
      apply_color(dest, color, endmask);
    }
  }
}

/**
 * Fill a square region. Optimized for non-pixel / non-line regions.
 */

void companion::Display::fill(const nxgl_rect_s& r, companion::Display::Color color)
{
  for (nxgl_coord_t i = r.pt1.x; i <= r.pt2.x; i++)
    vline({i,r.pt1.y}, r.pt2.y, color);
}

/**
 * Draw a rectangle. Optimized for non-pixel / non-line regions.
 * Assumes at least two pixels high.
 */
void companion::Display::rect(const nxgl_rect_s& r, companion::Display::Color color)
{
  vline({r.pt1.x,r.pt1.y}, r.pt2.y, color);
  vline({r.pt2.x,r.pt1.y}, r.pt2.y, color);
  hline({ (nxgl_coord_t)(r.pt1.x + 1), r.pt1.y}, (nxgl_coord_t)(r.pt2.x - 1), color);
  hline({ (nxgl_coord_t)(r.pt1.x + 1), r.pt2.y}, (nxgl_coord_t)(r.pt2.x - 1), color);
}

#define swap(x,y) { auto tmp = x; x = y; y = tmp; }

void companion::Display::line(nxgl_vector_s vect, Color color)
{
  nxgl_coord_t x0 = vect.pt1.x;
  nxgl_coord_t y0 = vect.pt1.y;
  nxgl_coord_t x1 = vect.pt2.x;
  nxgl_coord_t y1 = vect.pt2.y;

  nxgl_coord_t steep = abs(y1 - y0) > abs(x1 - x0);
  if (steep) {
    swap(x0, y0);
    swap(x1, y1);
  }

  if (x0 > x1) {
    swap(x0, x1);
    swap(y0, y1);
  }

  nxgl_coord_t dx, dy;
  dx = x1 - x0;
  dy = abs(y1 - y0);

  nxgl_coord_t err = dx / 2;
  nxgl_coord_t ystep;

  if (y0 < y1) {
    ystep = 1;
  } else {
    ystep = -1;
  }

  /* TODO: optimize for vlines / single pixels in a column */
  for (; x0<=x1; x0++) {
    if (steep) {
      pixel({y0, x0}, color);
    } else {
      pixel({x0, y0}, color);
    }
    err -= dy;
    if (err < 0) {
      y0 += ystep;
      err += dx;
    }
  }
}

/* TODO: return bounding box */
/* TODO: use cached fonts (to array of Bitmap's) */
nxgl_rect_s companion::Display::print(const char* text, nxgl_point_s pos, Font font, companion::Display::Color color)
{
  /* occupied bounding box */
  nxgl_rect_s bounding_box;
  bounding_box.pt1 = pos;
  bounding_box.pt2 = pos;

  /* get selected font handle */
  NXHANDLE font_handle = font_handles[(int)font];

  /* get information about the font we are going to use */
  const struct nx_font_s* fontset = nxf_getfontset(font_handle);

  for (const char* c = text; *c; c++)
  {
    const struct nx_fontbitmap_s* fbm = nxf_getbitmap(font_handle, *c);

    if (fbm)
    {
      uint16_t fwidth = fbm->metric.width + fbm->metric.xoffset;
      uint16_t fheight = fbm->metric.height + fbm->metric.yoffset;
      uint16_t fstride = (fwidth + 7) >> 3;

      size_t bufsize = fstride * fheight;

      uint8_t* glyph = (uint8_t*)malloc(bufsize);
      memset(glyph, 0, bufsize);

      nxf_convert_1bpp(glyph, fheight, fwidth, fstride, fbm, 1);

      //for (int i = 0; i < bufsize; i++) printf("in: %x\n", glyph[i]);

      /* Note: xoffset seems to be already compensated in previous call, thus I see an actual xoffset of 0 */
      Bitmap b(glyph, fstride, fwidth, fheight, 0 /*fbm->metric.xoffset*/, fbm->metric.yoffset);
      free(glyph);

      /* offset requested position by first character's offset */
      if (c == text)
      {
        pos.x -= b.offset().x;
        pos.y -= b.offset().y;
      }

      //for (int i = 0; i < b.bufsize(); i++) printf("out: %x\n", b.buffer()[i]);

      nxgl_rect_s bitmap_bb;
      bitmap(b, pos, &bitmap_bb);
      if (bitmap_bb.pt2.x > bounding_box.pt2.x) bounding_box.pt2.x = bitmap_bb.pt2.x;
      if (bitmap_bb.pt2.y > bounding_box.pt2.y) bounding_box.pt2.y = bitmap_bb.pt2.y;

#if 0
      /* test code for clearing background */
      nxgl_rect_s bitmap_box, bitmap_extents = b.extents();
      nxgl_rectoffset(&bitmap_box, &bitmap_extents, pos.x, pos.y);

      nxgl_point_s offset = b.offset();
      nxgl_point_s top_left = { pos.x + offset.x, pos.y + offset.y };
      nxgl_size_s real_size = b.real_size();
      printf("top left: %i %i offset: %i %i\n", top_left.x, top_left.y, fbm->metric.xoffset, fbm->metric.yoffset);
      nxgl_point_s bottom_right = {top_left.x + real_size.w - 1, top_left.y + real_size.h - 1};
      //fill({{0,0},{7,bottom_right.y}}, Color::INVERT);

      //vline({0,0},63, Color::INVERT);
      fill(bitmap_box, Color::INVERT);
#endif

      pos.x += fwidth + 1;
    }
    else
    {
      pos.x += fontset->spwidth;
    }
  }

  return bounding_box;
}


/**
 * Blit a bitmap into the display. This interprets the bitmap as a transparency mask:
 * on pixels are turned on, and off pixels are left the same. If this behavior is undesired
 * (i.e. the background should be cleared), you need to get the actual extents of the bitmap
 * and clear the background using a fill operation.
 */
void companion::Display::bitmap(const companion::Bitmap& b, const nxgl_point_s& pos,
                                nxgl_rect_s* bounding_box)
{
  /* obtain rectangle in window coordinates containing actual image */
  nxgl_rect_s bitmap_extents = b.full_extents(), bitmap_bb, clipped_bb;

  /*printf("bmp pos: %i %i\n", pos.x, pos.y);
  printf("bmp extents: %i %i %i %i\n", bitmap_extents.pt1.x, bitmap_extents.pt1.y,
         bitmap_extents.pt2.x, bitmap_extents.pt2.y);*/

  nxgl_rectoffset(&bitmap_bb, &bitmap_extents, pos.x, pos.y);
  nxgl_rectintersect(&clipped_bb, &bitmap_bb, &screen_rect);

  if (bounding_box) *bounding_box = clipped_bb;

  /*printf("bmp bb: %i %i %i %i\n", bitmap_bb.pt1.x, bitmap_bb.pt1.y,
         bitmap_bb.pt2.x, bitmap_bb.pt2.y);

  printf("clipped: %i %i %i %i\n", clipped_bb.pt1.x, clipped_bb.pt1.y,
         clipped_bb.pt2.x, clipped_bb.pt2.y);*/


  if (nxgl_nullrect(&clipped_bb)) return; /* nothing visible left */

  /* bitmap-relative coordinates to process */
  nxgl_point_s bitmap_origin = { clipped_bb.pt1.x - bitmap_bb.pt1.x, clipped_bb.pt1.y - bitmap_bb.pt1.y };
  nxgl_point_s bitmap_end = { bitmap_extents.pt2.x - (bitmap_bb.pt2.x - clipped_bb.pt2.x),
                              bitmap_extents.pt2.y - (bitmap_bb.pt2.y - clipped_bb.pt2.y) };

  /* find out how many bits to displace input to match output */
  uint8_t src_remainder = (bitmap_origin.y & 7);
  uint8_t dest_remainder = (clipped_bb.pt1.y & 7);
  uint8_t remainder = (src_remainder > dest_remainder ? src_remainder - dest_remainder : dest_remainder - src_remainder);
  bool lshift = (src_remainder < dest_remainder);

  /*printf("remainder: %i %i %i\n", src_remainder, dest_remainder, remainder);*/

  nxgl_coord_t cols = bitmap_end.x - bitmap_origin.x + 1;
  nxgl_coord_t rows = bitmap_end.y - bitmap_origin.y + 1;

  /*printf("bmp origin: %i %i bmp end: %i %i cols: %i rows: %i\n", bitmap_origin.x, bitmap_origin.y,
         bitmap_end.x, bitmap_end.y, cols, rows);*/

  for (int col = 0; col < cols; col++)
  {
    /* choose input and output bytes */
    const uint8_t* src_ptr = b.buffer() + (bitmap_origin.x + col) * b.stride() + (bitmap_origin.y >> 3);
    uint8_t* dst_ptr = fbmem + (clipped_bb.pt1.x + col) * stride + (clipped_bb.pt1.y >> 3);

    /* remember pointer to last byte to check bounds */
    const uint8_t* src_ptr_end = b.buffer() + (bitmap_origin.x + col) * b.stride() + (bitmap_end.y >> 3);
    uint8_t* dst_ptr_end = fbmem + (clipped_bb.pt1.x + col) * stride + (clipped_bb.pt2.y >> 3);

    /* TODO: above math could be simplified */

    uint8_t data = 0;

    for (int row = 0; row < rows; row += 8)
    {
      /* get bits aligned in input and move to align to output (note: ordering is LSB->MSB) */
      if (lshift)
      {
        data |= *src_ptr << remainder;

        if (data) *dst_ptr |= data; /* copy selected bits to output */

        /* last bits of current src byte left out in previous operation (reverse) */
        data = *src_ptr >> (8 - remainder);

        ++src_ptr;
        ++dst_ptr;
      }
      else
      {
        /* move up source pixels from this byte */
        data = *src_ptr >> remainder;

        /* and add the remaining bits from next byte (if there is one) */
        ++src_ptr;

        if (src_ptr <= src_ptr_end)
          data |= *src_ptr << (8 - remainder);

        /* write to output */
        if (data) *dst_ptr |= data;

        ++dst_ptr;
      }
    }

    /* if a few bits remained after last shift, also copy them */
    if (lshift && data && dst_ptr <= dst_ptr_end) *dst_ptr |= data;
  }
}


void companion::Display::bitmap(const uint8_t* data, const nxgl_rect_s& r, const nxgl_point_s& origin)
{
#if 0
  int width = ((rect.pt2.x - rect.pt1.x) + 1);
  uint8_t stride = (width + 7) >> 3;
  if (nx_bitmap(bkgd_handle, &rect, (const void**)&data, &origin, stride) < 0)
  {
    fprintf(stderr, "nx_bitmapwindow failed: %d\n", errno);
  }
#endif
}

void companion::Display::circle(const nxgl_point_s pos, nxgl_coord_t radius,
                                nxgl_coord_t start_section, nxgl_coord_t end_section,
                                companion::Display::Color color)
{
  /* TODO: complete this */

  nxgl_point_s pts[16];
  nxgl_circlepts(&pos, radius, pts);

  nxgl_point_s prev = pts[0];
  for (int i = start_section + 1; i < end_section; i++)
  {
    line({prev, pts[i]}, color);
    prev = pts[i];
  }

  if (start_section == 0 && end_section == 16)
    line({prev, pts[0]}, color);
  else
    line({prev, pts[end_section]}, color);
}

void companion::Display::filledcircle(const nxgl_point_s pos, nxgl_coord_t radius,
                                      nxgl_coord_t start_section, nxgl_coord_t end_section,
                                      companion::Display::Color color)
{
  /* TODO: complete this */
  nxgl_trapezoid_s traps[8];
  nxgl_circletraps(&pos, radius, traps);

  for (int i = 0; i < 8; i++)
  {
    trapezoid(traps[i], color);
  }
}

void companion::Display::trapezoid(const nxgl_trapezoid_s& trap, companion::Display::Color color)
{
  /* implement me */
}

void companion::Display::clear_memory(void)
{
  /* direct device access, not NX, this is faster */
  up_fbclear(0);
}

void companion::Display::clear(void)
{
  clear_memory();
  refresh();
}

void companion::Display::refresh()
{
  /* direct device access, not NX, this is faster */
  up_fbredraw(0);
}

uint8_t*companion::Display::framebuffer()
{
  return fbmem;
}

void companion::Display::debug()
{
  for (int i = 0; i < 8; i++)
  {
    for (int j = 0; j < 128; j++)
      printf("%x ", framebuffer()[i * 8 + j]);
  }
  printf("\n");
}



void companion::Display::load_fonts(void)
{
  font_handles[(int)Font::MONO_SMALL] = nxf_getfonthandle(FONTID_MONO5X8);
  font_handles[(int)Font::SANS_MEDIUM] = nxf_getfonthandle(FONTID_SANS23X27);
  font_handles[(int)Font::SANS_BIG] = nxf_getfonthandle(FONTID_SANS39X48);
}

