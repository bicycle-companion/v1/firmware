#!/usr/bin/ruby

def reverse_byte(i)
  r = 0
  8.times do
    r = (r << 1) | i & 1
    i = i >> 1
  end
  return r
end

s = File.open(ARGV.first,'r') {|f| f.read}
s = s.gsub(/0[xX][0-9a-fA-F]{2}/) {|match| '0x' + reverse_byte(match.to_i(16)).to_s(16).upcase }

File.open(ARGV.first, 'w') { |f| f.write(s) }
