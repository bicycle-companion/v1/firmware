SHELL:=/bin/bash

TARGET:=sim
CONFIG:=nsh

all: build 

.PHONY: tools modules_link drivers_link configure clean

mconf:
	cd NuttX/tools/kconfig-frontends && \
		./configure --prefix=/usr --enable-mconf --disable-gconf --disable-qconf && \
		make && make install

modules_link:
	ln -snf ../../modules NuttX/apps/external

configure: 
	cd NuttX/nuttx/tools && ./configure.sh ${TARGET}/${CONFIG}

menuconfig: tools modules_link
	cd NuttX/nuttx && source ./setenv.sh && ${MAKE} menuconfig

depend:
	cd NuttX/nuttx && source ./setenv.sh && ${MAKE} depend

tools:
	${MAKE} -C NuttX/nuttx/tools -f Makefile.host

build: tools modules_link 
	cd NuttX/nuttx && source ./setenv.sh && ${MAKE}

clean:
	cd NuttX/nuttx && source ./setenv.sh && ${MAKE} clean

download: tools modules_link
	cd NuttX/nuttx && source ./setenv.sh && ${MAKE} download
	
